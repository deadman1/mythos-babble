#!/usr/bin/python3 
 
## MIT License 
# 
# Copyright (c) 2023 Don Coffin 
# 
# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this code and associated documentation files (the "Software"), 
# to deal in the Software without restriction, including without 
# limitation the rights to use, copy, modify, merge, publish, 
# distribute, sublicense, and/or sell copies of the Software, and to 
# permit persons to whom the Software is furnished to do so, subject to 
# the following conditions: 
# 
# The above copyright notice and this permission notice shall be 
# included in all copies or substantial portions of the Software. 

# mythos-babble.py 
# A simple script for generating evocative-sounding "Mythos babble", 
# inspired by the "techno babble" of Star Trek. 
 
import random 
 
# Define lists of words 
 
verbs = ["amplify", "analyze", "attune to", "bind", "calculate", "channel", "conjure", "decrypt", "derive", "distort", "encode", "engage", "evoke", "extrapolate", "harmonize", "illuminate", "integrate", "intertwine", "manifest", "modulate", "optimize", "oscillate", "penetrate", "project", "quantify", "resonate", "reveal", "simulate", "transmute", "traverse"] 
 
adjectives = ["abyssal", "arcane", "astral", "celestial", "chthonic", "corporeal", "ecliptic", "eldritch", "ephemeral", "fractal", "lunar", "mystical", "necrotic", "quantum", "radiant", "retrograde", "sidereal", "tensor", "umbral", "venusian", "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "twenty-third", "Einstein", "Bohr", "Curie", "Heisenberg", "Schrödinger", "Planck", "Hubble", "Crowley", "Waite", "Steiner", "Alhazred", "Carter", "Whately", "Armitage"] 
 
nouns = ["algorithm", "ascendant", "conjunction", "crystal", "cusp", "eigenvalue", "ecliptic", "entanglement", "flux", "harmonic", "heuristic", "neutrino", "orbital", "protocol", "ritual", "sigil", "singularity", "vortex", "waveform", "zenith", "zodiac"] 
 
# Randomly choose a word from each category 
def generate_term(): 
    verb = random.choice(verbs) 
    adjective = random.choice(adjectives) 
    noun = random.choice(nouns) 
 
    mythos_babble = verb + " the " + adjective + " " + noun 
 
    return mythos_babble 
 
# Spit it out
 
print (generate_term()) 
