# Mythos Babble

## Description
A very simple Python script for generating evocative "Mythos babble". The generated phrases are meant to be said by Call of Cthulhu NPCs, found in the journals of wild scientists, and so on. Some sample output of the script includes:

```
manifest the fifth waveform
quantify the Carter vortex
channel the tenth eigenvalue
decrypt the eleventh flux
attune to the abyssal zenith
reveal the corporeal ecliptic
distort the radiant flux
bind the Whately neutrino
reveal the Heisenberg entanglement
reveal the venusian algorithm
```

## Installation
It's a Python script, so you'll need a Python interpreter to run it. Alas, the installation of a working Python environment is beyond the ken of this project.

## Usage
Download or copy the script to someplace with a working Python environment, then run the script from the commandline:

```
bash$ ./mythos-babble.py
```

## License
This software is released under the MIT license. Please see the LICENSE file or the script itself for more information.

